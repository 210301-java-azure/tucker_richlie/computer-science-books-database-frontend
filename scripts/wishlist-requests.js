const myToken = sessionStorage.getItem("token");

let splitToken = myToken.split("-");
let email = splitToken[3];

attemptGetWishlistBooks(email, myToken);

document.getElementById("add-new-wishlist-form").addEventListener("submit", attemptAddNewBookToWishlist);
document.getElementById("has-read-wishlist-form").addEventListener("submit", attemptUpdateHasRead);

function attemptGetWishlistBooks(email, myToken) {
	getWishList(email, successGetWishlistBooks, failGetWishlistBooks, myToken);
}


function successGetWishlistBooks(xhr) {
	const wishlist = JSON.parse(xhr.responseText);
	if (wishlist.length) {
		renderWishlistBooksInTable(wishlist);
	}
	else {
		renderWishlistInTable(wishlist);
	}
}

function failGetWishlistBooks(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
}


function attemptAddNewBookToWishlist(event) {
	event.preventDefault();
	const bookTitle = document.getElementById("book-title").value;
	const newWishlistBook = {"book": bookTitle, "hasRead": false, "user": email};
	postNewBookToWishList(newWishlistBook, successAddNewBookToWishlist, failAddNewBookToWishlist, myToken);
}

function successAddNewBookToWishlist() {
	attemptGetWishlistBooks(email, myToken);

}

function failAddNewBookToWishlist() {
	const message = document.getElementById("hidden-message"); 
    message.hidden = false;

}

function attemptUpdateHasRead(event) {
	event.preventDefault();
	const bookTitle = document.getElementById("book-title-has-read").value;
	const hasReadWishlistBook = {"book": bookTitle, "hasRead": true, "user": email};
	putHasReadTrue(hasReadWishlistBook, email, successUpdateHasRead, failUpdateHasRead, myToken);
}

function successUpdateHasRead() {
	attemptGetWishlistBooks(email, myToken);

}

function failUpdateHasRead() {
	const message = document.getElementById("hidden-message-has-read"); 
    message.hidden = false;

}


function renderWishlistBooksInTable(books) {
	const table = document.getElementById("wishlist-table").hidden = false;
	const tableBody = document.getElementById("table-body-wishlist");
	while (tableBody.hasChildNodes()) {
		tableBody.removeChild(tableBody.firstChild);
	}
	for (let book of books) {
		let newRow = document.createElement("tr");
		newRow.innerHTML = `<td>${book.hasRead}</td><td>${book.book.title}</td>`;
		tableBody.appendChild(newRow);
	}
}

function renderWishlistInTable(book) {
	const table = document.getElementById("wishlist-table").hidden = false;
	const tableBody = document.getElementById("table-body-wishlist");
	let newRow = document.createElement("tr");
	newRow.innerHTML = `<td>${book.hasRead}</td><td>${book.book.title}</td>`;
	tableBody.appendChild(newRow);
}


