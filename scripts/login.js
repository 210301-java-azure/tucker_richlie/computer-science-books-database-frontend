
const loginForm = document.getElementById("login-form").addEventListener("submit", attemptUserLogin);

function attemptUserLogin(event) {
	event.preventDefault(); //this stops the form from creating submitting an GET request by default to the same url
	const email = document.getElementById("input-email").value;
	const password = document.getElementById("input-password").value;
	userLogin(email, password, successfulUserLogin, userLoginFailure);
}

function successfulUserLogin(xhr) {
	console.log("login successful");
	const authToken = xhr.getResponseHeader("Authorization");
	sessionStorage.setItem("token", authToken);
	window.location.href="../views/index.html";
}

function userLoginFailure(xhr) {
	const errorText = document.getElementById("login-error");
	errorText.hidden = false;
	errorText.innerText = xhr.responseText;


}