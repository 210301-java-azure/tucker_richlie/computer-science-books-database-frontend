const genreToken = sessionStorage.getItem("token");

if (genreToken && genreToken.length == 16) {
	authenticateAdminLogin(genreToken);
}


attemptGetAllGenres();

document.getElementById("add-new-genre-form").addEventListener("submit", attemptAddNewGenre);
document.getElementById("delete-genre-form").addEventListener("submit", attemptDeleteGenre);
document.getElementById("update-genre-form").addEventListener("submit", attemptUpdateGenreDescription);


function authenticateAdminLogin(token){
    authorizeAdminToken(isAdminToken, notAdminToken, genreToken);
}


function isAdminToken(xhr) {
	if (xhr.status == 200) {
		document.getElementById("genre-add-button").hidden = false;
		document.getElementById("genre-delete-button").hidden = false;
		document.getElementById("genre-update-button").hidden = false;
		document.getElementById("login-button").hidden = true;
		document.getElementById("logout-button").hidden = false;
		
	}
}


function notAdminToken(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	console.log("invalid token");
}

function attemptGetAllGenres() {
	getAllGenres(getGenresSuccess, getGenresFailure);
}


function getGenresSuccess(xhr) {
	const genres = JSON.parse(xhr.responseText);
	renderGenresInTable(genres);

}

function getGenresFailure(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	document.getElementById("error-text").hidden = false;

}

function attemptAddNewGenre(event) {

	event.preventDefault();
	const genreTitle = document.getElementById("genre-title-add").value;
	const description = document.getElementById("description").value;
	const newGenre = {"genreTitle": genreTitle, "description": description};
	postNewGenre(newGenre, successAddNewGenre, failAddNewGenre, genreToken);
}

function successAddNewGenre() {
	attemptGetAllGenres();

}

function failAddNewGenre() {
	const message = document.getElementById("hidden-message-add"); 
    message.hidden = false;

}

function attemptDeleteGenre() {

	event.preventDefault();
	const genreTitle = document.getElementById("genre-title-delete").value;
	deleteGenre(genreTitle, successDeleteGenre, failDeleteGenre, genreToken);
}

function successDeleteGenre() {
	attemptGetAllGenres();

}

function failDeleteGenre() {
	const message = document.getElementById("hidden-message-delete"); 
    message.hidden = false;

}

function attemptUpdateGenreDescription() {

	event.preventDefault();
	const genreTitle = document.getElementById("genre-title-update").value;
	const newDescription = document.getElementById("update-description").value;
	const descriptionObject = {"description": newDescription};
	putGenreDescription(genreTitle, descriptionObject, successDeleteGenre, failDeleteGenre, genreToken);
}

function successUpdateGenre() {
	attemptGetAllGenres();

}

function failUpdateGenre() {
	const message = document.getElementById("hidden-message-update"); 
    message.hidden = false;

}




function renderGenresInTable(genres) {
	const table = document.getElementById("genre-table").hidden = false;
	const tableBody = document.getElementById("table-body-genres");
	while (tableBody.hasChildNodes()) {
		tableBody.removeChild(tableBody.firstChild);
	}
	for (let genre of genres) {
		let newRow = document.createElement("tr");
		newRow.innerHTML = `<td>${genre.genreTitle}</td><td>${genre.description}`;
		tableBody.appendChild(newRow);
	}
}


