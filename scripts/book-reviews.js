
const myToken = sessionStorage.getItem("token");

let splitToken = myToken.split("-");
let email = splitToken[3];

attemptGetAllBookReviews(email, myToken);

document.getElementById("add-new-review-form").addEventListener("submit", attemptAddNewBookReview);

function attemptGetAllBookReviews(email, myToken) {
	getAllBookReviews(email, successGetAllBookReviews, failGetAllBookReviews, myToken);
}


function successGetAllBookReviews(xhr) {
	const bookReviews = JSON.parse(xhr.responseText);
	if (bookReviews.length) {
		renderReviewsInTable(bookReviews);
	}
	else {
		renderReviewInTable(bookReviews);
	}
}

function failGetAllBookReviews(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	const errorMessage = document.getElementById("error-message-book-review");
	errorMessage.hidden = false;
}


function attemptAddNewBookReview(event) {

	event.preventDefault();
	const bookTitle = document.getElementById("book-title").value;
	const rating = document.getElementById("rating").value;
	const newBookReview = {"book": bookTitle, "rating": rating, "user": email};
	postNewBookReview(newBookReview, successAddNewBookReview, failAddNewBookReview, myToken);
}

function successAddNewBookReview() {
	const errorMessage = document.getElementById("error-message-book-review");
	errorMessage.hidden = true;
	attemptGetAllBookReviews(email, myToken);

}

function failAddNewBookReview() {
	const message = document.getElementById("hidden-message"); 
    message.hidden = false;

}


function renderReviewsInTable(books) {
	const table = document.getElementById("book-review-table").hidden = false;
	const tableBody = document.getElementById("table-body-book-reviews");
	while (tableBody.hasChildNodes()) {
		tableBody.removeChild(tableBody.firstChild);
	}
	for (let book of books) {
		let newRow = document.createElement("tr");
		newRow.innerHTML = `<td>${book.rating}</td><td>${book.book.title}</td>`;
		tableBody.appendChild(newRow);
	}
}

function renderReviewInTable(book) {
	const table = document.getElementById("book-review-table").hidden = false;
	const tableBody = document.getElementById("table-body-book-reviews");
	let newRow = document.createElement("tr");
	newRow.innerHTML = `<td>${book.rating}</td><td>${book.book.title}</td>`;
	tableBody.appendChild(newRow);
}

