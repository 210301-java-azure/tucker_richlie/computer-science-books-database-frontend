const submitRegisterUser = document.getElementById("register-user").addEventListener("submit", registerNewUser);


function registerNewUser(event) {
	event.preventDefault();
	const firstName = document.getElementById("input-first-name").value;
	const lastName = document.getElementById("input-last-name").value;
	const name = firstName + " " + lastName;
	const email = document.getElementById("input-email").value;
	const password = document.getElementById("input-password").value;
	const confirmPassword = document.getElementById("confirm-password").value;
	if (password == confirmPassword) {
		const newUser = {"name": name, "password": password, "email": email};
		postNewUser(newUser, successfulRegisterOfNewUser, registrationFailure);
	}
	else {
		const passwordError = document.getElementById("password-error");
		passwordError.hidden = false;
		passwordError.innerText = "Passwords do not match";
	}
}

function successfulRegisterOfNewUser() {
	console.log("registration successful");
	window.location.href = "../views/login.html";
}

function registrationFailure(xhr) {
	const emailError = document.getElementById("email-error");
	emailError.hidden = false;
	emailError.innerText = xhr.responseText;
}