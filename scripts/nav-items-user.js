
// DON'T FORGET TO IMPLEMENT CHECK IF TOKEN IS VALID

// Take this token and check it against your server to make sure that it is a valid token
const token = sessionStorage.getItem("token");

if (token && token.length == 16) {
	authenticateAdminLogin(token);
}
else if (token) {
	validateUserToken();
}


function authenticateAdminLogin(token){
    authorizeAdminToken(isAdminToken, notAdminToken, token);
}


function isAdminToken(xhr) {
	if (xhr.status == 200) {
		
		document.getElementById("login-button").hidden = true;
		document.getElementById("logout-button").hidden = false;
		
	}
}


function notAdminToken(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	console.log("invalid token");
}

//Assuming for now that if not admin token then user token --- fix later to check for user validation
function validateUserToken() {
	getAllUserEmails(isUserToken, invalidToken)
		
}

function isUserToken(xhr) {
	const emails = JSON.parse(xhr.responseText);
	for (let email of emails) {
		userToken = "user-auth-token-" + email;
		if (userToken == token) {
			console.log("login successful");
			document.getElementById("login-button").hidden = true;
    		document.getElementById("book-review").hidden = false;
    		document.getElementById("wishlist").hidden = false;
    		document.getElementById("user-account").hidden=false;
   			document.getElementById("logout-button").hidden = false;
   			break;
		}
	}
}

function invalidToken(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	console.log("invalid token");
}




document.getElementById("logout-button").addEventListener("click", logout);

function logout() {
	sessionStorage.removeItem("token");
	document.getElementById("login-button").hidden = false;
    document.getElementById("book-review").hidden = true;
    document.getElementById("wishlist").hidden = true;
    document.getElementById("user-account").hidden=true;
    document.getElementById("logout-button").hidden = true;

}

