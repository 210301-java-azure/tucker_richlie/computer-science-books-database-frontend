
const myToken = sessionStorage.getItem("token");

let splitToken = myToken.split("-");
let email = splitToken[3];

displayUserInfo(email, myToken);

const deleteUserListener = document.getElementById("delete-button").addEventListener("click", deleteUser);
const updateNameListener = document.getElementById("update-name-btn").addEventListener("click", changeUserName);

function changeUserName() {
	const name = document.getElementById("update-name").value;
	const newName = {"name": name};
	console.log(newName);
	changeNameOfUser(email, newName, successChangeUserName, failChangeUserName, myToken);

}

function successChangeUserName() {
	displayUserInfo(email, myToken);
	console.log("Name successfully updated!")

}

function failChangeUserName(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	console.log("something went wrong");
}

function deleteUser() {
	deleteUserAccount(email, successDeleteUser, failureDeleteUser, myToken);
}

function successDeleteUser(xhr) {
	logout();
	window.location.href="../views/login.html";
}

function failureDeleteUser(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
}

function displayUserInfo(email, myToken) {
	getUserInfo(email, successDisplayUserInfo, failureDisplayUserInfo, myToken);
}


function successDisplayUserInfo(xhr) {
	const user = JSON.parse(xhr.responseText);
	const name = document.getElementById("user-name");
	const email = document.getElementById("user-email");
	name.innerText = `Name: ${user.name}`;
	email.innerText = `Email: ${user.email}`;
}

function failureDisplayUserInfo(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	document.getElementById("error-text").hidden = false;
}



