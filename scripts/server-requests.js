
function sendRequest(method, url, successCallback, failureCallback, body, authToken) {
	const xhr = new XMLHttpRequest();
	xhr.open(method, url);
	if (authToken) {
		xhr.setRequestHeader("Authorization", authToken);
	}
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			if (xhr.status > 199 && xhr.status < 300) {
				successCallback(xhr);
			}

			else {
				failureCallback(xhr);
			}
		}
		
	}
	if (body) {
		xhr.send(body);
	}
	else {
		xhr.send();
	}
}

function adminLogin(username, password, successCallback, failureCallback) {
	const payload = `username=${username}&password=${password}`;
	sendRequest("POST", "http://51.141.163.182:80/admin-login", successCallback, failureCallback, payload);
}

function authorizeAdminToken(successCallback, failureCallback, authToken) {
	sendRequest("GET", "http://51.141.163.182:80/authorize-admin-token", successCallback, failureCallback, undefined, authToken);
}


function userLogin(username, password, successCallback, failureCallback) {
	const payload = `email=${username}&password=${password}`;
	sendRequest("POST", "http://51.141.163.182:80/user-login", successCallback, failureCallback, payload);
}

function getAllBooks(successCallback, failureCallback) {
	sendRequest("GET", "http://51.141.163.182:80/books", successCallback, failureCallback);
}

function getAllUserEmails(successCallback, failureCallback) {
	sendRequest("GET", "http://51.141.163.182:80/users/email", successCallback, failureCallback);
}

function postNewUser(newUser, successCallback, failureCallback) {
	const userJSON = JSON.stringify(newUser);
	sendRequest("POST", "http://51.141.163.182:80/register-user", successCallback, failureCallback, userJSON);
}

function getBookByTitle(title, successCallback, failureCallback) {
	sendRequest("GET", `http://51.141.163.182:80/books/title/${title}`, successCallback, failureCallback);
}

function getBooksByAuthor(author, successCallback, failureCallback) {
	sendRequest("GET", `http://51.141.163.182:80/books/author/${author}`, successCallback, failureCallback);
}

function getBooksByPublisher(publisher, successCallback, failureCallback) {
	sendRequest("GET", `http://51.141.163.182:80/books/publisher/${publisher}`, successCallback, failureCallback);
}

function getBookByISBN(isbn, successCallback, failureCallback) {
	sendRequest("GET", `http://51.141.163.182:80/books/isbn/${isbn}`, successCallback, failureCallback);
}

function getBooksByGenre(genre, successCallback, failureCallback) {
	sendRequest("GET", `http://51.141.163.182:80/books/genre/${genre}`, successCallback, failureCallback);
}

function getAllGenres(successCallback, failureCallback) {
	sendRequest("GET", "http://51.141.163.182:80/genres", successCallback, failureCallback);
}

function getUserInfo(email, successCallback, failureCallback, authToken) {
	sendRequest("GET", `http://51.141.163.182:80/users/${email}`, successCallback, failureCallback, undefined, authToken);
}

function deleteUserAccount(email, successCallback, failureCallback, authToken) {
	sendRequest("DELETE", `http://51.141.163.182:80/users/${email}`, successCallback, failureCallback, undefined, authToken);
}

function changeNameOfUser(email, name, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(name);
	sendRequest("PUT", `http://51.141.163.182:80/users/${email}`, successCallback, failureCallback, payload, authToken);}

function getAllBookReviews(email, successCallback, failureCallback, authToken) {
	sendRequest("GET", `http://51.141.163.182:80/users/book-reviews/${email}`, successCallback, failureCallback, undefined, authToken);
}

function postNewBookReview(book, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(book);
	sendRequest("POST", "http://51.141.163.182:80/users/book-reviews", successCallback, failureCallback, payload, authToken);
}

function getWishList(email, successCallback, failureCallback, authToken) {
	sendRequest("GET", `http://51.141.163.182:80/users/wishlist/${email}`, successCallback, failureCallback, undefined, authToken);
}

function postNewBookToWishList(book, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(book);
	console.log(payload);
	sendRequest("POST", "http://51.141.163.182:80/users/wishlist", successCallback, failureCallback, payload, authToken);

}

function putHasReadTrue(book, email, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(book);
	sendRequest("PUT", `http://51.141.163.182:80/users/wishlist/${email}`, successCallback, failureCallback, payload, authToken);
}

function postNewGenre(genre, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(genre);
	sendRequest("POST", "http://51.141.163.182:80/admin/genre", successCallback, failureCallback, payload, authToken);
}


function deleteGenre(genreTitle, successCallback, failureCallback, authToken) {
	sendRequest("DELETE", `http://51.141.163.182:80/admin/genre/${genreTitle}`, successCallback, failureCallback, undefined, authToken);
}


function putGenreDescription(genreTitle, description, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(description);
	sendRequest("PUT", `http://51.141.163.182:80/admin/genre/${genreTitle}`, successCallback, failureCallback, payload, authToken);
}


function postNewBook(book, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(book);
	console.log(payload);
	sendRequest("POST", "http://51.141.163.182:80/admin/books", successCallback, failureCallback, payload, authToken);
}

function addNewAuthor(author, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(author);
	sendRequest("POST", "http://51.141.163.182:80/admin/author", successCallback, failureCallback, payload, authToken);
}

function deleteBook(title, successCallback, failureCallback, authToken) {
	sendRequest("DELETE", `http://51.141.163.182:80/admin/books/${title}`, successCallback, failureCallback, undefined, authToken);
}

function putNewBookDescription(title, description, successCallback, failureCallback, authToken) {
	const payload = JSON.stringify(description);
	sendRequest("PUT", `http://51.141.163.182:80/admin/books/description/${title}`, successCallback, failureCallback, payload, authToken);
}

function putNewBookPublisher(title, publisher, successCallback, failureCallback, authToken) {
		const payload = JSON.stringify(publisher);
		sendRequest("PUT", `http://51.141.163.182:80/admin/books/publisher/${title}`, successCallback, failureCallback, payload, authToken);
}

