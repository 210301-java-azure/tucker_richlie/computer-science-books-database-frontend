
const loginForm = document.getElementById("login-form").addEventListener("submit", attemptAdminLogin);


function attemptAdminLogin(event) {
	event.preventDefault(); //this stops the form from creating submitting an GET request by default to the same url
	const username = document.getElementById("input-admin-username").value;
	const password = document.getElementById("input-admin-password").value;
	adminLogin(username, password, successfulAdminLogin, adminLoginFailure);
}

function successfulAdminLogin(xhr) {
	console.log("login successful");
	const authToken = xhr.getResponseHeader("Authorization");
	sessionStorage.setItem("token", authToken);
	window.location.href="../views/index.html";
}

function adminLoginFailure(xhr) {
	const errorText = document.getElementById("login-error");
	errorText.hidden = false;
	errorText.innerText = xhr.responseText;


}