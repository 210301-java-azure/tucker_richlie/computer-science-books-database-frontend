const bookToken = sessionStorage.getItem("token");

if (bookToken && bookToken.length == 16) {
	authenticateAdminLogin(bookToken);
}

attemptGetAllBooks();
document.getElementById("toggle-search-criteria").addEventListener("click", toggleSearchCriteriaFunction);
document.getElementById("hide-search-criteria").addEventListener("click", hideSearchCriteriaFunction);
document.getElementById("title-btn").addEventListener("click", searchBooksByTitle);
document.getElementById("author-btn").addEventListener("click", searchBooksByAuthor);
document.getElementById("publisher-btn").addEventListener("click", searchBooksByPublisher);
document.getElementById("isbn-btn").addEventListener("click", searchBooksByISBN);
document.getElementById("genre-btn").addEventListener("click", searchBooksByGenre);

document.getElementById("add-new-book-form").addEventListener("submit", attemptAddNewBook);
document.getElementById("delete-book-form").addEventListener("submit", attemptDeleteBook);
document.getElementById("update-description-book-form").addEventListener("submit", attemptUpdateBookDescription);
document.getElementById("update-publisher-book-form").addEventListener("submit", attemptUpdateBookPublisher);

function authenticateAdminLogin(token){
    authorizeAdminToken(isAdminToken, notAdminToken, bookToken);
}


function isAdminToken(xhr) {
	if (xhr.status == 200) {
		document.getElementById("book-add-button").hidden = false;
		document.getElementById("book-delete-button").hidden = false;
		document.getElementById("book-update-button").hidden = false;
		document.getElementById("login-button").hidden = true;
		document.getElementById("logout-button").hidden = false;
		
	}
}


function notAdminToken(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	console.log("invalid token");
}

function attemptGetAllBooks() {
	getAllBooks(successGetAllBooks, failGetAllBooks);
}

function successGetAllBooks(xhr) {
	const books = JSON.parse(xhr.responseText);
	renderBooksInTable(books);
}

function failGetAllBooks(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	document.getElementById("error-text-general").hidden = false;

}


function renderBooksInTable(books) {
	const table = document.getElementById("book-table").hidden = false;
	const tableBody = document.getElementById("table-body-books");
	while (tableBody.hasChildNodes()) {
		tableBody.removeChild(tableBody.firstChild);
	}
	for (let book of books) {
		let newRow = document.createElement("tr");
		let stringGenre = "";
		let stringAuthors = "";
		console.log(stringAuthors);
		let numGenres = book.genres.length;
		let numAuthors = book.authors.length;
		for (genre of book.genres) {
			if (numGenres == 1) {
				stringGenre = stringGenre.concat(genre.genreTitle);
			}
			else {
				stringGenre = stringGenre.concat(genre.genreTitle, ",");
			}
		}

		for (author of book.authors) {
			if (numAuthors == 1) {
				stringAuthors = stringAuthors.concat(author.name);
			}
			else {
				stringAuthors = stringAuthors.concat(author.name, ",");
			}
		}

		newRow.innerHTML = `<td>${book.title}</td><td>${book.publisher}<td>${book.date}<td>${book.isbn}</td><td>${book.description}</td><td>${book.rating}</td><td>${stringGenre}</td><td>${stringAuthors}</td>`;
		tableBody.appendChild(newRow);
	}
}

function renderBookInTable(book) {
	const table = document.getElementById("book-table").hidden = false;
	const tableBody = document.getElementById("table-body-books");
	while (tableBody.hasChildNodes()) {
		tableBody.removeChild(tableBody.firstChild);
	}
	let newRow = document.createElement("tr");
	let stringGenre = "";
		let stringAuthors = "";
		console.log(stringAuthors);
		let numGenres = book.genres.length;
		let numAuthors = book.authors.length;
		for (genre of book.genres) {
			if (numGenres == 1) {
				stringGenre = stringGenre.concat(genre.genreTitle);
			}
			else {
				stringGenre = stringGenre.concat(genre.genreTitle, ",");
			}
		}

		for (author of book.authors) {
			if (numAuthors == 1) {
				stringAuthors = stringAuthors.concat(author.name);
			}
			else {
				stringAuthors = stringAuthors.concat(author.name, ",");
			}
		}
	newRow.innerHTML = `<td>${book.title}</td><td>${book.publisher}<td>${book.date}<td>${book.isbn}</td><td>${book.description}</td><td>${book.rating}</td><td>${stringGenre}</td><td>${stringAuthors}</td>`;
	tableBody.appendChild(newRow);
}

function toggleSearchCriteriaFunction() {
	const criteria = document.getElementsByClassName("search-criteria");
	const searchButton = document.getElementsByClassName("search-btn");
	for (let i = 0; i < criteria.length; i++) {
		criteria[i].hidden = false;
		searchButton[i].hidden = false;
	}
	document.getElementById("hide-search-criteria").hidden = false;
	document.getElementById("toggle-search-criteria").hidden = true;
}

function hideSearchCriteriaFunction() {
	const table = document.getElementById("book-table").hidden = true;
	const criteria = document.getElementsByClassName("search-criteria");
	const searchButton = document.getElementsByClassName("search-btn");
	const searchErrors = document.getElementsByClassName("search-error");
	for (let i = 0; i < criteria.length; i++) {
		criteria[i].hidden = true;
		searchButton[i].hidden = true;
		searchErrors[i].hidden = true;
	}
	document.getElementById("hide-search-criteria").hidden = true;
	document.getElementById("toggle-search-criteria").hidden = false;
	const tableBody = document.getElementById("table-body-books");
	while (tableBody.hasChildNodes()) {
		tableBody.removeChild(tableBody.firstChild);
	}
	attemptGetAllBooks();
}


function searchBooksByTitle() {
	const title = document.getElementById("search-by-title").value;
	getBookByTitle(title, titleSuccess, titleFailure);
}

function titleSuccess(xhr) {
	const book = JSON.parse(xhr.responseText);
	renderBookInTable(book);
	document.getElementById("error-text-title").hidden = true;
}

function titleFailure(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	document.getElementById("error-text-title").hidden = false;
}

function searchBooksByAuthor() {
	const author = document.getElementById("search-by-author").value;
	getBooksByAuthor(author, authorSuccess, authorFailure);
}

function authorSuccess(xhr) {
	const books = JSON.parse(xhr.responseText);
	renderBooksInTable(books);
	document.getElementById("error-text-author").hidden = true;
}

function authorFailure(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	document.getElementById("error-text-author").hidden = false;
}

function searchBooksByPublisher() {
	const publisher = document.getElementById("search-by-publisher").value;
	getBooksByPublisher(publisher, publisherSuccess, publisherFailure);
}

function publisherSuccess(xhr) {
	const books = JSON.parse(xhr.responseText);
	renderBooksInTable(books);
	document.getElementById("error-text-publisher").hidden = true;
}

function publisherFailure(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	document.getElementById("error-text-publisher").hidden = false;
}

function searchBooksByISBN() {
	const isbn = document.getElementById("search-by-isbn").value;
	getBookByISBN(isbn, isbnSuccess, isbnFailure);
}

function isbnSuccess(xhr) {
	const books = JSON.parse(xhr.responseText);
	renderBookInTable(books);
	document.getElementById("error-text-isbn").hidden = true;
}

function isbnFailure(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	document.getElementById("error-text-isbn").hidden = false;
}

function searchBooksByGenre() {
	const genre = document.getElementById("search-by-genre").value;
	getBooksByGenre(genre, genreSuccess, genreFailure);
}

function genreSuccess(xhr) {
	const books = JSON.parse(xhr.responseText);
	renderBooksInTable(books);
	document.getElementById("error-text-genre").hidden = true;
}

function genreFailure(xhr) {
	console.log(xhr.readyState);
	console.log(xhr.status);
	document.getElementById("error-text-genre").hidden = false;
}

function attemptAddNewBook(event) {

	event.preventDefault();
	const bookTitle = document.getElementById("book-title-add").value;
	const authors = document.getElementById("book-author-add").value;
	const authorsArray = authors.split(", ");
	console.log(authorsArray);
	const description = document.getElementById("book-description-add").value;
	const isbn = document.getElementById("book-isbn-add").value;
	const publisher = document.getElementById("book-publisher-add").value;
	const date = document.getElementById("book-date-add").value;
	const genres = document.getElementById("book-genre-add").value;
	const genresArray = genres.split(", ");
	for (author of authorsArray) {
		const newAuthor = {"name": author};
		console.log(newAuthor);
		addNewAuthor(newAuthor, authorSuccessCallback, authorFailCallback, bookToken);
	}
	const newBook = {"title": bookTitle, "publisher": publisher, "date": date, "isbn": isbn, "description": description, "rating": 0, "genres": genresArray, "authors": authorsArray};
	postNewBook(newBook, successAddNewBook, failAddNewBook, bookToken);
}

function successAddNewBook() {
	attemptGetAllBooks();
}

function failAddNewBook() {
	const message = document.getElementById("hidden-message-add"); 
    message.hidden = false;
}


function authorSuccessCallback() {
	console.log("New Author added");
}

function authorFailCallback() {
	console.log("Author already in db, won't add new author");
}


function attemptDeleteBook(event) {
	event.preventDefault();
	const bookTitle = document.getElementById("book-title-delete").value;
	deleteBook(bookTitle, successDeleteBook, failureDeleteBook, bookToken);


}

function successDeleteBook() {
	attemptGetAllBooks();
	console.log("book deleted");
}

function failureDeleteBook() {
	const message = document.getElementById("hidden-message-delete"); 
    message.hidden = false;
}

function attemptUpdateBookDescription(event) {
	event.preventDefault();
	const bookTitle = document.getElementById("book-title-update-description").value;
	const newDescription = document.getElementById("book-description-update-description").value;
	const newDescriptionObject = {"description": newDescription};
	putNewBookDescription(bookTitle, newDescriptionObject, successUpdateBookDescription, failUpdateBookDescription, bookToken);
}

function successUpdateBookDescription() {
	attemptGetAllBooks();
}

function failUpdateBookDescription() {
	const message = document.getElementById("hidden-message-update-description"); 
    message.hidden = false;
}

function attemptUpdateBookPublisher(event) {
	event.preventDefault();
	const bookTitle = document.getElementById("book-title-update-publisher").value;
	const newPublisher = document.getElementById("book-publisher-update-publisher").value;
	const newPublisherObject = {"publisher": newPublisher};
	putNewBookPublisher(bookTitle, newPublisherObject, successUpdateBookPublisher, failUpdateBookPublisher, bookToken);
}

function successUpdateBookPublisher() {
	attemptGetAllBooks();
}

function failUpdateBookPublisher() {
	const message = document.getElementById("hidden-message-update-publisher"); 
    message.hidden = false;
}






